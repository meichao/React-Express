const express = require('express');
const bodyParser = require('body-parser');
const proxy = require('http-proxy-middleware');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConf = require('./webpack.config');
var compiler = webpack(webpackConf);

var isDev = process.env.NODE_ENV == 'development'?true:false;
var app = new express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(isDev){
    var context = '/api';
    var options = {
        target: 'http://bigmeichao.com/',//目标服务器地址
        changeOrigin: true,             //虚拟主机网站需要
    }
    var apiProxy =  proxy(context, options);
    app.use(apiProxy);
}
app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConf.output.publicPath,
    progress: true,
    stats: {
        colors: true,
        chunks: false
    }
}));
app.use(webpackHotMiddleware(compiler));
app.listen(webpackConf.devServer.port);