1：pm2多进程守护暂时感觉还不需要,pm2开启进程守护适合在服务器上架设node服务器这样操作

2：babel-register貌似暂时不需要用到

3:node的版本使用的是7.8.0的,一定要注意这个问题，如果直接在webstorm的命令行中npm install的话，默认的node的版本是6.10.0的

4:把热更新很慢的问题解决了,时间降下来了，原先热更新的时候需要10几秒，现在最多两三秒,主要的措施是:
       1：react、react-dom单独拎出来
       2:exclude: function(path) {
             // 路径中含有 node_modules 的就不去解析。
             var isNpmModule = !!path.match(/node_modules/);
             return isNpmModule;
         },
         3:webpack.optimize.UglifyJsPlugin 代码压缩不发生在开发阶段，仅仅是在发布阶段才有

5:需要加一个拦截器


 收获:今天最大的收获就是webpack热更新的极致优化



 需要学习:redux,react-router4