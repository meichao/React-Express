/*
 * Created by MC on 2017/10/16.
 */
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var htmlWebpackPlugin = require('html-webpack-plugin');
require("babel-polyfill");
var node_modules = path.resolve(__dirname, 'node_modules');
var isDev = process.env.NODE_ENV == 'development'?true:false;
// 原先是打算用react 16的,但是后来发现在调试源代码的时候看到的js文件不是初始的js项目,就又换成了15的
// var pathToReact = path.resolve(node_modules,isDev? 'react/umd/react.development.js':'react/umd/react.production.min.js');
// var pathToReactDom = path.resolve(node_modules,isDev? 'react-dom/umd/react-dom.development.js':'react-dom/umd/react-dom.production.min.js');
var pathToReact = path.resolve(node_modules, 'react/dist/react.min.js');
var pathToReactDom = path.resolve(node_modules, 'react-dom/dist/react-dom.min.js');
//var pathToReactRouter = path.resolve(node_modules,'react-router/dist/react-router.min.js');
var config = {
    entry:{
        previewDo: ['babel-polyfill'],
        bound:[
                path.resolve(__dirname, './src/routes')
            ],
        react:['react','react-dom','react-router'],
        // antd:['antd']
    },
    output: {
        path: path.resolve(__dirname, './build'),
        filename: 'js/[name].js',
        // publicPath:'http://localhost:8181/',         //同样可以
        publicPath:isDev?'/':'/rateConfig/',            //非开发版的路径还有待确认,等文文那边确认
        chunkFilename: "js/[name]-[chunkhash:6].bundle.min.js"
    },
    devServer:{
        // contentBase: '',  //静态资源的目录 相对路径,相对于当前路径 默认为当前config所在的目录
        // devtool: 'eval',
        // hot: true,        //自动刷新
        inline: true,
        port: 8282
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: function(path) {
                    // 路径中含有 node_modules 的就不去解析。
                    var isNpmModule = !!path.match(/node_modules/);
                    return isNpmModule;
                },
                use:{
                    loader: 'babel-loader'
                }
            },{
                test: /\.(css|less)$/,
                use:ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use:['css-loader','less-loader','postcss-loader']
                })
            },{
                test: /\.(png|jpg|jpeg|gif|eot|woff|svg|ttf|woff2|appcache)(\?|$)/,
                exclude: /^node_modules$/,
                use: 'file-loader?name=img/[name].[ext]'
            }
        ]
    },
    resolve:{
        // 文件路径指向(可加快打包过程)
        // alias: {
        //     'react': pathToReact,
        //     'react-dom':pathToReactDom,
        //     //'react-router':pathToReactRouter
        // },
        extensions: ['*', '.js', '.jsx','scss','css'], //后缀名自动补全
    },
    /*
    *   HtmlWebpackPlugin 这里会自动生成css和js文件,自己在build文件下创建的css文件和js文件目前未用到,但是先不删除,以防后期有变
    *   有模板html文件,这样原先的 页面跳转了，title不能改变的问题
    */
    plugins: [
        // 把react-react-dom单独拿出来，不参与打包,但是需要在页面上使用script标签引入对应的js文件
        // new webpack.ProvidePlugin({
        //     React: 'react',
        //     ReactDOM: 'react-dom',
        // }),
        // new webpack.optimize.OccurenceOrderPlugin(),                //按引用频度来排序 ID，以便达到减少文件大小的效果
        new webpack.HotModuleReplacementPlugin(),                   //开启全局的 HMR 能力
        // new webpack.optimize.CommonsChunkPlugin('react'),
        new webpack.optimize.CommonsChunkPlugin({
            name: ["react", "previewDo"],//页面上使用的时候common2
            //必须最先加载
            // filename:"chunk.js"//忽略则以name为输出文件的名字，
            //否则以此为输出文件名字
            minChunks: 2
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendors', // 将公共模块提取，生成名为`vendors`的chunk
        //     chunks: ['react','antd'], //提取哪些模块共有的部分
        //     minChunks: 2 // 提取至少3个模块共有的部分
        // }),
        new ExtractTextPlugin("css/[name].[contenthash:6].css"),
        new htmlWebpackPlugin({
            filename: 'index.html',
            template: `${__dirname}/build/index.html`,
            inject: 'body',
            hash: false,
            // chunks: ['vendors', 'bound']
        })
    ]
};
if(!isDev){
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        output: {
        comments: false,
        },
        compress: {
        warnings: false
        }
    }));
    // 文件路径指向(可加快打包过程)，开发的时候想要调试下react的源代码,就不引用整合好的react了
    config.resolve.alias = {
        'react': pathToReact,
        'react-dom':pathToReactDom,
        //'react-router':pathToReactRouter
    };
}else{
    // 来分析 Webpack 生成的包体组成并且以可视化的方式反馈给开发者
    var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    config.plugins.push(new BundleAnalyzerPlugin());

    // 开发环境添加资源映射,方便调试
    config.devtool = 'source-map';

    config.entry.previewDo.unshift('webpack-hot-middleware/client?path=/__webpack_hmr&reload=true');
}
module.exports = config

/*
*   antd没必要引进来,因为antd是按需加载的
*/